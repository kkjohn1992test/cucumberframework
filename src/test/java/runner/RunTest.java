package runner;

import org.testng.annotations.Test;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


/**
 * @author CHIRAG
 *
 */

// Uncomment @RunWith if you are using Junit to run Test 
// @RunWith(Cucumber.class)
//features={"src//test//java//features"

@CucumberOptions(   features={"src//test//java//features"},
					glue={"stepdefinations","utility"}
					,plugin = {"pretty"}
		)
@Test
public class RunTest extends AbstractTestNGCucumberTests{

}
